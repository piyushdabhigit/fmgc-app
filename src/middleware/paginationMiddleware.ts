import { Request, Response, NextFunction } from 'express';

interface Pagination {
  page: number;
  limit: number;
  skip: number;
}

// Extend the Request interface to include the pagination property
declare global {
  namespace Express {
    interface Request {
      pagination: Pagination;
    }
  }
}

export const paginate = (req: Request, res: Response, next: NextFunction): void => {
  const page: number = parseInt(req.query.page as string, 10) || 1;
  const limit: number = parseInt(req.query.limit as string, 10) || 10;
  const skip: number = (page - 1) * limit;

  req.pagination = {
    page,
    limit,
    skip,
  };

  next();
};
