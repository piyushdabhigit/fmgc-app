import { Request, Response, NextFunction } from 'express';

declare global {
  namespace Express {
    interface Response {
      success(data: any, message?: string, statusCode?: number): Response;
      error(message: string, statusCode?: number, data?: any): Response;
    }
  }
}

const responseBuilder = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Extend res object with custom methods if not already extended
  if (!res.success) {
    res.success = function(data: any, message = 'Success', statusCode = 200) {
      return res.status(statusCode).json({
        success: true,
        message,
        data,
      });
    };
  }

  if (!res.error) {
    res.error = function(message: string, statusCode = 500, data?: any) {
      const errorResponse: any = { success: false, message };
      if (data) {
        errorResponse.data = data;
      }
      return res.status(statusCode).json(errorResponse);
    };
  }

  // Call the next middleware
  next();
};

export default responseBuilder;
