import { Request, Response, NextFunction } from 'express';

// Middleware to check if the user is an admin
export const isAdmin: any = (req: Request, res: Response, next: NextFunction) => {
    if (req.user.role !== 'admin') {
        res.error('Access denied. Only admins can access this route.', 403);
    }
    next();  
};
