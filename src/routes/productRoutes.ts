import express from 'express';
import { check } from 'express-validator';
import { createProduct,getProduct,updateProduct,deleteProduct } from '../controllers/productController';
import { authenticateUser } from '../middleware/authMiddleware';
import { isAdmin } from '../middleware/roleMiddleware'; 
const router = express.Router();

// Middleware to authenticate user for protected routes
router.use(authenticateUser);

// Route to create a new product (admin only)
router.post(
  '/product', 
  [
    check('name', 'Product name is required').notEmpty(),
    check('description', 'Product description is required').notEmpty(),
    check('price', 'Product price must be a number').isNumeric(),
    check('category', 'Category ID is required').notEmpty(),
    check('upc', 'Product UPC is required').notEmpty(),
  ],
  createProduct
);

// Route to create a new product (admin only)
router.get(
    '/product',isAdmin,
    [
        check('name').optional().isString().withMessage('Name must be a string'),
        check('upc').optional().isString().withMessage('UPC must be a string'),
        check('categoryName').optional().isString().withMessage('Category name must be a string'),  
    ],
    getProduct
);

// Route to update product details
router.put(
    '/product/:productId',
    [
        check('name').optional().isString().withMessage('Name must be a string'),
        check('description').optional().isString().withMessage('Description must be a string'),
        check('price').optional().isNumeric().withMessage('Price must be a number'),
        check('category').optional().isMongoId().withMessage('Invalid category ID'),
        check('upc').optional().isString().withMessage('UPC must be a string')
    ],
    updateProduct
);

// Route to delete a product
router.delete(
    '/product/:productId',
    deleteProduct
);

export default router;
