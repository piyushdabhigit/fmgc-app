import mongoose, { Document, Schema } from 'mongoose';

export interface User {
  username: string;
  email: string;
  password: string;
  role: 'user' | 'admin';
}

export interface UserDocument extends User, Document {}

const userSchema = new Schema<UserDocument>({
  username: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  role: { type: String, enum: ['user', 'admin'], default: 'user' }
}, { timestamps: true }); // Automatically adds createdAt and updatedAt fields

const UserModel = mongoose.model<UserDocument>('User', userSchema);

export default UserModel;
